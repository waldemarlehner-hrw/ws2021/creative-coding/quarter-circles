package de.waldemarlehner.quartercircles;

import de.waldemarlehner.javaListenerUtils.Func;
import processing.core.PApplet;
import processing.core.PVector;

import java.awt.*;
import java.util.Random;

public class Main extends PApplet
{
    // Settings
    private final int tileCount = 20;
    private final float emptyTileFraction = 0.2f;
    private final int gap = 10;
    private final int tileSize = 20;
    private final int seed = 1337;

    private final int hueDegrees = 180;
    private final int hueDegreesDeviation = 20;

    private final Func.OneArg<Float, Float> DistanceToSizeFunction = ( Float distance) -> {
        var value = Math.cos( distance / (50* Math.PI) ) * 1.2f;
        if(distance > 250) {
            return 0f;
        }
        if(value < 0) {
            value = 0;
        }
        if(value > 1) {
            value = 1;
        }
        return (float)value;
    };

    // End
    private Tile[][] drawables;

    @Override
    public void settings()
    {
        var size = tileCount * tileSize + gap * (tileCount - 1);
        super.size(size, size);
    }

    @Override
    public void setup()
    {
        super.noStroke();

        Random rand = new Random(this.seed);

        drawables = new Tile[tileCount][tileCount];

        for(int x = 0; x < tileCount;x++)
        {

            for( int y = 0; y < tileCount; y++) {
                if(rand.nextFloat() < this.emptyTileFraction) {
                    continue;
                }
                var hue = this.hueDegrees + ((rand.nextFloat() * 2) - 1) * this.hueDegreesDeviation;

                var color = Color.getHSBColor( hue / 360f, 1, 1 );
                var arcOrientation = rand.nextInt() % 4;
                if(arcOrientation < 0) {
                    arcOrientation *= -1;
                }
                var drawable = new Tile( tileSize, color, arcOrientation);
                this.drawables[x][y] = drawable;
            }
        }
    }

    @Override
    public void draw()
    {
        background( 0xFFFFFF );
        PVector mouse = new PVector(mouseX, mouseY);


        for(int x = 0; x < tileCount;x++)
        {
            for( int y = 0; y < tileCount; y++) {
                var drawable = this.drawables[x][y];
                if(drawable == null) {
                    continue;
                }

                var positionOfDrawable = new PVector((x) * (tileSize + gap) + tileSize / 2f, (y) * (tileSize + gap) + tileSize / 2f);
                var distance = mouse.dist( positionOfDrawable );
                var modifier = this.DistanceToSizeFunction.invoke(distance);
                drawable.setCompletedPosition( modifier );
                drawable.draw( this, (x) * (tileSize + gap), (y) * (tileSize + gap) );

            }
        }
    }

    // Program entry point
    public static void main( String[] args )
    {
        PApplet.main( Main.class, args );
    }

}
