package de.waldemarlehner.quartercircles;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PVector;

import java.awt.*;

public class Tile
{
    private final int color;
    private float mouseDistanceModifier = 1;
    private final int size;
    private int arcOrientation = 0;

    public Tile( int size, Color color ) {
        this.size = size;
        this.color = color.getRGB();
    }

    public Tile( int size, Color color, int arcOrientation ) {
        this( size, color );
        this.arcOrientation = arcOrientation;
    }


    public void setCompletedPosition( float percentage) {
        this.mouseDistanceModifier = percentage;
    }

    public void draw(PApplet applet, int xOffset, int yOffset) {
        this.drawArc( applet, xOffset, yOffset );
    }

    private void drawRectangle( PApplet applet, int xOffset, int yOffset ) {
        if(mouseDistanceModifier == 0) {
            return;
        }
        var activeSize = size * mouseDistanceModifier;
        var localLowerX = (size / 2f) - (activeSize/2);
        var localLowerY = (size / 2f) - (activeSize/2);

        applet.fill( this.color );
        applet.rect( localLowerX + xOffset, localLowerY+ yOffset, activeSize, activeSize );
    }

    private void drawArc(PApplet applet, int xOffset, int yOffset) {
        if(mouseDistanceModifier == 0) {
            return;
        }

        var activeSize = size;
        var localLowerX = (size / 2f) - (activeSize/2);
        var localLowerY = (size / 2f) - (activeSize/2);
        if(this.arcOrientation == 1 || this.arcOrientation == 2) {
            localLowerX += size;
        }
        if(this.arcOrientation >= 2) {
            localLowerY += size;
        }

        applet.fill( this.color );

        var start = PConstants.PI / 2 * this.arcOrientation;
        var stop = start + (PConstants.PI / 2) * this.mouseDistanceModifier;

        applet.arc( localLowerX + xOffset, localLowerY+ yOffset, 2*activeSize, 2*activeSize, start, stop );
    }
}
